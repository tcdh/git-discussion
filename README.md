Git Discussion
===========

### book

 [https://git-scm.com/book/de/v1](https://git-scm.com/book/de/v1) (thank to @schmitza for the tip)

### basic

repository == directory

commit == recorded changes

branch == directions of changes

#### files status:

untracked, commited, stashed

### basic commands

```
git init
git add filename
git add -A (--all) (stage all untracked and modified files)
git commit -m "message"
git checkout -b branchName
git checkout master
git branch (show all local branches)
git log (with parameters in next discussion time)
git config --global user.email email
git config --global user.name username

git merge targetBranch

git reset --hard (change HEAD and branch pointers and files)
git reset --soft 
(only change HEAD and branch pointers, useful for squash)
squash: combine more than one commits in one commit

git init --bare  == create private git server
git clone (--recursive) url
git remote -v
git remote add origin url
git push origin branchName
git pull origin branchName
git pull --abort (if there are complex confilcts)

(for complex repository with frequent changes)
git fetch
git diff master origin/master
.... (work with local changes)
git merge origin/master

git diff branchname(commit) branchname(commit) -- filename
git diff --tool=nameoftool .....
git config diff.tool toolname
(will be discussed further in part "resolving conflict")
```

### resolving conflict (to be continued...)


